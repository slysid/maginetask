###############################################
# Docker file for MAGINE
###############################################

###############################################
#Base OS Image
###############################################
FROM centos

###############################################
# File Author
###############################################
MAINTAINER Bharath


###############################################
# Safe Update - can be ignored
###############################################
RUN yum update -y

###############################################
#Create Working Directory
###############################################
ENV HOME  /opt/magnine
RUN mkdir $HOME
WORKDIR $HOME

###############################################
#Install Package Managers
###############################################
RUN yum install -y wget
RUN wget http://peak.telecommunity.com/dist/ez_setup.py
RUN python ez_setup.py
RUN easy_install pip
RUN yum install -y vim



###############################################
#Install Application Dependencies
###############################################
RUN yum groupinstall -y "Development Tools"
RUN yum install -y python-devel
RUN pip install requests
RUN pip install flask
RUN pip install psutil

###############################################
#Copy App
###############################################
COPY app app
COPY server.py server.py


###############################################
#Port Config
###############################################
EXPOSE 5000

###############################################
#Entry Point
###############################################
ENTRYPOINT python server.py

