from app import app
import json
from flask import request

statLog = '/tmp/data.json'

@app.route('/api/health',methods=['GET'])
def health():
      if request.method == 'GET':
          return json.dumps({'status':'Alive'})
     

@app.route('/api/stat',methods=['POST'])
def stat():
      if request.method == 'POST':
            with open(statLog,'w') as f:
                f.write(request.data)
      return "{}"