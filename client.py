import psutil as psu
import datetime
import time
import os
import platform
import requests
import json
from requests.exceptions import ConnectionError

userAgreement = False
usageDetail = {}
delay = 10
statHost = 'localhost'
statPort = '5000'
statMethod = '/api/stat'
healthMethod = '/api/health'
baseURL = 'http://' + statHost + ':' +statPort
statURL = baseURL + statMethod
healthURL = baseURL + healthMethod

def getPermission():
    global userAgreement
    
    response = raw_input('Send stats to help improve product(y/n):')
    if response.lower() == 'y':
         userAgreement = True
         
def generateStats(pid):
     global usageDetail
     
     usageDetail['time'] = datetime.datetime.strftime(datetime.datetime.now(),'%b %d %Y %H:%M:%S')
     usageDetail['pid'] = pid
     usageDetail['cpu'] = psu.cpu_count()
     process = psu.Process(pid)
     usageDetail['system'] = {}
     usageDetail['system']['cpuPercent'] = psu.cpu_percent()
     usageDetail['system']['cpuTime'] = psu.cpu_times()
     usageDetail['process'] = {}
     usageDetail['process']['cpuPercent'] = process.cpu_percent()
     usageDetail['process']['cpuTime'] = process.cpu_times()
     usageDetail['process']['memory'] = process.memory_info()
     usageDetail['process']['connections'] = process.connections()
     usageDetail['process']['threadCount'] = process.num_threads()
     if platform.system().lower() != 'darwin':
         usageDetail['process']['io'] = process.io_counters()
     
def sendStat():
     requests.post(statURL,data=json.dumps(usageDetail))
     
def checkHealth():
     try:
         response = requests.get(healthURL)
         if 'Alive' in response.text:
           return True
         return False
     except requests.exceptions.ConnectionError as err:
         return False
     
     
         
if __name__ == '__main__':
    
    getPermission()
    
    while userAgreement:
        if checkHealth() == True:
            pid = os.getpid()
            generateStats(pid)
            sendStat()
            print 'Stat posted'
        else:
            print 'Stat collection server is down'
        time.sleep(delay)
            
            