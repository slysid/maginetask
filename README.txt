#### client.py ####

client.py simulates the client running.It's role includes

1.Get user permission
2.Check for Stat collection service.
3.If running collect stats and post it to stat service
4.If not running keep quiet.

How to run the client
python client.py


#### server.py ####

server.py exposes the apis needed. It exposes two apis

/api/health - which checks the health of self.
/api/stat - which post the stats

How to run the client
python server.py

The default port on which the server will run is 5000

The server has a docker file which can dockerise the server